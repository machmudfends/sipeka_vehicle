<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vehicle;

class VehicleController extends Controller
{
    public function index(){
        $vehicle = Vehicle::where('status', '1')->get();
        return view('auth.master.vehicles.index', [
            'vehicles' => $vehicle
        ]);
    }

    public function pageNew(){
        return view('auth.master.vehicles.new');
    }

    public function postNew(Request $request){
        $rules = [
            'name' => 'required',
            'type' => 'required',
            'ownership' => 'required',
            'fuel_consumption' => 'required',
            'service_every' => 'required',
            'service_calendar' => 'required'
        ];
        $messages = [
            'name.required' => 'Nama Wajib diisi',
            'type.required' => 'Tipe Wajib diisi',
            'ownership.required' => 'Kepemilikan Wajib diisi',
            'fuel_consumption.required' => 'Konsumsi BBM Wajib diisi',
            'service_every.required' => 'Service Wajib diisi',
            'service_calendar.required' => 'Service Wajib diisi',
        ];

        $request->validate($rules, $messages);

        Vehicle::create([
            'name' => $request->name,
            'type' => $request->type,
            'ownership' => $request->ownership,
            'fuel_consumption' => $request->fuel_consumption,
            'service_every' => $request->service_every,
            'service_calendar' => $request->service_calendar,
            'status' => '1'
        ]);

        return redirect()->route('new.vehicles')->with('success', 'Berhasil menambahkan kendaraan '.$request->name);
    }

    public function pageEdit($id){
        $vehicle = Vehicle::find($id);
        return view('auth.master.vehicles.edit', [
            'vehicle' => $vehicle
        ]);
    }

    public function postEdit(Request $request, $id){
        $rules = [
            'name' => 'required',
            'type' => 'required',
            'ownership' => 'required',
            'fuel_consumption' => 'required',
            'service_every' => 'required',
            'service_calendar' => 'required'
        ];
        $messages = [
            'name.required' => 'Nama Wajib diisi',
            'type.required' => 'Tipe Wajib diisi',
            'ownership.required' => 'Kepemilikan Wajib diisi',
            'fuel_consumption.required' => 'Konsumsi BBM Wajib diisi',
            'service_every.required' => 'Service Wajib diisi',
            'service_calendar.required' => 'Service Wajib diisi',
        ];

        $request->validate($rules, $messages);

        $vehicle = Vehicle::find($id);

        if($vehicle){
            $vehicle->name = $request->name;
            $vehicle->type = $request->type;
            $vehicle->ownership = $request->ownership;
            $vehicle->fuel_consumption = $request->fuel_consumption;
            $vehicle->service_every = $request->service_every;
            $vehicle->service_calendar = $request->service_calendar;
            $vehicle->save();
            return redirect()->route('index.vehicles')->with('success', 'Berhasil menubah data kendaraan '.$request->name);
        }else{
            return redirect()->route('index.vehicles')->with('error', 'Tidak menemukan data kendaraan');
        }
    }

    public function delete($id){
        $vehicle = Vehicle::find($id);
        if($vehicle){
            $vehicle->status = "0";
            $vehicle->save();
            return redirect()->route('index.vehicles')->with('success', 'Berhasil menghapus data kendaraan '.$vehicle->name.'. Buka menu bin untuk menampilkan data yang terhapus');
        }else{
            return redirect()->route('index.vehicles')->with('error', 'Tidak menemukan data kendaraan');
        }
    }

    public function bulkDelete($ids){
        $ids = explode(',',$ids);
        
        $vehicle = Vehicle::whereIn("id", $ids)
        ->update([
            "status" => "0"
        ]);

        if($vehicle){
            return redirect()->route('index.vehicles')->with('success', 'Berhasil menghapus data kendaraan. Buka menu bin untuk menampilkan data yang terhapus');
        }else{
            return redirect()->route('index.vehicles')->with('error', 'Tidak menemukan data kendaraan');
        }
    }
}
