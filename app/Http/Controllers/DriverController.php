<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Driver;
use Validator;

class DriverController extends Controller
{
    public function index(){
        $driver = Driver::where('status', '1')->get();
        return view('auth.master.drivers.index', [
            'drivers' => $driver
        ]);
    }

    public function pageNew(){
        return view('auth.master.drivers.new');
    }

    public function postNew(Request $request){
        $rules = [
            'nik' => 'required|unique:drivers,nik',
            'name' => 'required',
        ];
        $messages = [
            'nik.required' => 'NIK Wajib diisi',
            'nik.unique' => 'NIK sudah terdaftar',
            'name.required' => 'Nama Wajib diisi',
        ];
        // dd($request->name);

        $request->validate($rules, $messages);

        Driver::create([
            'nik' => $request->nik,
            'name' => $request->name,
            'phone' => $request->phone,
            'status' => '1'
        ]);

        return redirect()->route('new.drivers')->with('success', 'Berhasil menambahkan pengendara '.$request->name);
    }

    public function pageEdit($id){
        $driver = Driver::find($id);
        return view('auth.master.drivers.edit', [
            'driver' => $driver
        ]);
    }

    public function postEdit(Request $request, $id){
        $rules = [
            'nik' => 'required|unique:drivers,nik,'.$id,
            'name' => 'required',
        ];
        $messages = [
            'nik.required' => 'NIK Wajib diisi',
            'nik.unique' => 'NIK sudah terdaftar',
            'name.required' => 'Nama Wajib diisi',
        ];
        // dd($request->name);

        $request->validate($rules, $messages);

        $driver = Driver::find($id);

        if($driver){
            $driver->nik = $request->nik;
            $driver->name = $request->name;
            $driver->phone = $request->phone;
            $driver->save();
            return redirect()->route('index.drivers')->with('success', 'Berhasil menubah data pengemudi '.$request->name);
        }else{
            return redirect()->route('index.drivers')->with('error', 'Tidak menemukan data pengemudi');
        }
    }

    public function delete($id){
        $driver = Driver::find($id);
        if($driver){
            $driver->status = "0";
            $driver->save();
            return redirect()->route('index.drivers')->with('success', 'Berhasil menghapus data pengemudi '.$driver->name.'. Buka menu bin untuk menampilkan data yang terhapus');
        }else{
            return redirect()->route('index.drivers')->with('error', 'Tidak menemukan data pengemudi');
        }
    }

    public function bulkDelete($ids){
        $ids = explode(',',$ids);
        
        $driver = Driver::whereIn("id", $ids)
        ->update([
            "status" => "0"
        ]);

        if($driver){
            return redirect()->route('index.drivers')->with('success', 'Berhasil menghapus data pengemudi. Buka menu bin untuk menampilkan data yang terhapus');
        }else{
            return redirect()->route('index.drivers')->with('error', 'Tidak menemukan data pengemudi');
        }
    }
}
