<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Booking;
use App\Models\Approver;
use App\Models\Vehicle;
use App\Models\Driver;
use App\Models\User;
use Auth;

class BookingController extends Controller
{
    public function index(){
        $user = Auth::user();
        
        if($user->role == 0){
            $bookings = Booking::select('bookings.*','v.name as name_vehicle', 'd.name as name_driver')
                ->where('id_admin', $user->id)
                ->join('vehicles as v', 'bookings.id_vehicle', '=', 'v.id')
                ->join('drivers as d', 'bookings.id_driver', '=', 'd.id')
                ->orderBy('updated_at', 'desc')
                ->get();

            foreach($bookings as $key => $booking){
                $approver = Approver::select('approvers.*', 'u.name as name_approver')
                    ->join('users as u', 'approvers.id_approver', '=', 'u.id')
                    ->where('id_booking',$booking->id)
                    ->get();
                $bookings[$key]['approval'] = $approver;
            }
            // print "<pre>";
            // print_r($bookings[0]["approval"][0]->id);
            // print "</pre>";die();
        }else{
            $bookings = Booking::select('bookings.*','v.name as name_vehicle', 'd.name as name_driver')
                ->where('a.status','!=' ,0)
                ->join('vehicles as v', 'bookings.id_vehicle', '=', 'v.id')
                ->join('drivers as d', 'bookings.id_driver', '=', 'd.id')
                ->join('approvers as a', function($join) use ($user) {
                    $join->on('bookings.id', '=', 'a.id_booking')
                        ->where('id_approver', $user->id);
                })
                ->orderBy('updated_at', 'desc')
                ->get();
            
            foreach($bookings as $key => $booking){
                $approver = Approver::select('approvers.*', 'u.name as name_approver')
                    ->join('users as u', 'approvers.id_approver', '=', 'u.id')
                    ->where('id_booking',$booking->id)
                    ->get();
                $bookings[$key]['approval'] = $approver;
            }
        }

        // dd($bookings);
        return view('auth.master.bookings.index', [
            'bookings' => $bookings
        ]);
    }

    public function approve($id){
        $approver = Approver::find($id);

        Approver::find($approver->id)
            ->update([
                'status' => '2'
            ]);
        
        $nextapprover = Approver::where('id_booking', $approver->id_booking)
            ->where('count_order', $approver->count_order + 1)
            ->first();

        if(!$nextapprover){
            // finish approval
            Booking::find($approver->id_booking)
                ->update([
                    'status' => '1'
                ]);
            
            return redirect()->route('index.bookings')->with('success', 'Berhasil menyetujui, Persetujuan telah selesai');
        }else{
            $driver = Approver::find($nextapprover->id)
                ->update([
                    'status' => '1'
                ]);
            return redirect()->route('index.bookings')->with('success', 'Berhasil menyetujui, dan mengirim ke penyetuju berikutnya');
        }
    }

    public function reject($id){
        $approver = Approver::find($id);

        Approver::find($approver->id)
            ->update([
                'status' => '3'
            ]);
        
        Booking::find($approver->id_booking)
            ->update([
                'status' => '2'
            ]);

        $nextapprover = Approver::where('id_booking', $approver->id_booking)
            ->where('count_order', $approver->count_order + 1)
            ->first();

        if(!$nextapprover){
            // finish approval
            return redirect()->route('index.bookings')->with('success', 'Berhasil Menolak, Persetujuan telah selesai');
        }else{
            $driver = Approver::find($nextapprover->id)
                ->update([
                    'status' => '0'
                ]);
            return redirect()->route('index.bookings')->with('success', 'Berhasil Menolak, Persetujuan telah selesai');
        }
    }

    public function pageNew(){
        $vehicles = Vehicle::where('status', '1')->get();
        $drivers = Driver::where('status', '1')->get();
        $approvers = User::where('role', '1')
            ->where('status', '1')
            ->get();

        return view('auth.master.bookings.new', [
            'vehicles' => $vehicles,
            'drivers' => $drivers,
            'approvers' => $approvers,
        ]);
    }

    public function postNew(Request $request){
        $rules = [
            'vehicle' => 'required',
            'driver' => 'required',
            'approver' => 'required|array|min:2',
        ];
        $messages = [
            'vehicle.required' => 'Kendaraan Wajib diisi',
            'driver.unique' => 'Pengendara sudah terdaftar',
            'approver.required' => 'Penyetuju Wajib diisi',
            'approver.array' => 'Pilih Beberapa Persetujuan',
            'approver.min' => 'Minimal pilih 2 persetujuan',
        ];

        $request->validate($rules, $messages);

        $user = Auth::user();

        $booking = Booking::create([
            'id_vehicle' => $request->vehicle,
            'id_driver' => $request->driver,
            'id_admin' => $user->id,
            'status' => '0'
        ]);

        foreach($request->approver as $key => $approver){
            Approver::create([
                'id_booking' => $booking->id,
                'id_approver' => $approver,
                'count_order' => $key + 1,
                'status' => $key == 0 ? 1 : 0,
            ]);
        }

        return redirect()->route('new.drivers')->with('success', 'Berhasil menambahkan pengendara '.$request->name);
    }
}
