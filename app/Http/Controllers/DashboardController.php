<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Booking;
use App\Models\Driver;
use App\Models\Vehicle;
use App\Models\User;

class DashboardController extends Controller
{
    public function index(){
        // $startdate = Carbon::now()->startOfYear();
        // $enddate = Carbon::now()->endOfYear(); 

        $user = User::where('role', '1')->get();
        $driver = Driver::all();
        $vehicle = Vehicle::all();

        $chart = [];
        foreach(range(1, 12) as $index){

            $startdate = Carbon::createFromFormat('m/Y', $index.'/'.date('Y'))->startOfMonth();
            $enddate = Carbon::createFromFormat('m/Y', $index.'/'.date('Y'))->endOfMonth();

            $bookings = Booking::select('bookings.*','v.name as name_vehicle', 'd.name as name_driver', 'u.name as name_admin')
                    ->whereBetween('bookings.created_at', [$startdate, $enddate])
                    ->join('vehicles as v', 'bookings.id_vehicle', '=', 'v.id')
                    ->join('drivers as d', 'bookings.id_driver', '=', 'd.id')
                    ->join('users as u', 'bookings.id_admin', '=', 'u.id')
                    ->orderBy('updated_at', 'desc')
                    ->get();
            $chart[$index - 1] = $bookings->count();
        }

        $startdate = Carbon::now()->startOfYear();
        $enddate = Carbon::now()->endOfYear();

        $booking = Booking::select('bookings.*','v.name as name_vehicle', 'd.name as name_driver', 'u.name as name_admin')
                    ->whereBetween('bookings.created_at', [$startdate, $enddate])
                    ->join('vehicles as v', 'bookings.id_vehicle', '=', 'v.id')
                    ->join('drivers as d', 'bookings.id_driver', '=', 'd.id')
                    ->join('users as u', 'bookings.id_admin', '=', 'u.id')
                    ->orderBy('updated_at', 'desc')
                    ->get();

        return view('auth/dashboard', [
            'charts' => json_encode($chart),
            'approver' => $user->count(),
            'driver' => $driver->count(),
            'vehicle' => $vehicle->count(),
            'booking' => $booking->count(),
        ]);
    }
}
