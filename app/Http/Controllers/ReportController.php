<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Booking;
use App\Models\Approver;
use App\Exports\BookingVehicleExport;
use Maatwebsite\Excel\Facades\Excel;

use Carbon\Carbon;

class ReportController extends Controller
{
    public function bookingVehicle(Request $request){
        if(isset($request->d)){
            $date = explode("-",str_replace(' ','',$request->d));
            $startdate = Carbon::createFromFormat('m/d/Y', $date[0]);
            $enddate = Carbon::createFromFormat('m/d/Y', $date[1]);
            // dd($startdate . ' - '.$enddate);
        }else{
            $startdate = Carbon::now()->startOfMonth();
            $enddate = Carbon::now()->endOfMonth(); 
        }

        $bookings = Booking::select('bookings.*','v.name as name_vehicle', 'd.name as name_driver', 'u.name as name_admin')
                ->whereBetween('bookings.created_at', [$startdate, $enddate])
                ->join('vehicles as v', 'bookings.id_vehicle', '=', 'v.id')
                ->join('drivers as d', 'bookings.id_driver', '=', 'd.id')
                ->join('users as u', 'bookings.id_admin', '=', 'u.id')
                ->orderBy('updated_at', 'desc')
                ->get();

        // dd($bookings);
        foreach($bookings as $key => $booking){
            $approver = Approver::select('approvers.*', 'u.name as name_approver')
                ->join('users as u', 'approvers.id_approver', '=', 'u.id')
                ->where('id_booking',$booking->id)
                ->get();
            $bookings[$key]['approval'] = $approver;
            $bookings[$key]['approval_count'] = $approver->count();
        }

        // dd($booking->count());
        return view('auth.master.reports.bookingvehicle', [
            'bookings' => $bookings,
            'startdate' => $startdate,
            'enddate' => $enddate
        ]);
    }

    public function excel(Request $request){
        return Excel::download(new BookingVehicleExport($request->d), 'pemesanan-kendaraan.xlsx');
    }
}
