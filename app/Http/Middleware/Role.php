<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Http\Request;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ... $roles)
    {
        if (!Auth::check())
            return redirect('login');

        $user = Auth::user();
    
        foreach($roles as $role) {
            // dd($role);
            // dd($user->hasRole($role));
            if($user->hasRole($role))
                return $next($request);
        }
    
        return redirect('login');
    }
}
