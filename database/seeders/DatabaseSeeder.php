<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Str;
use Hash;  
use Faker\Factory as Faker;
use App\Models\User;
use App\Models\Driver;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $user = 'admin1,0#admin2,0#admin3,0#approver1,1#approver2,1#approver3,1';

        $faker = Faker::create();

        foreach (explode('#',$user) as $first){
            // first it be "admin 1,admin1,0" 
            User::create([
                'name' => $faker->name,
                'username' => explode(',', $first)[0],
                'password' => Hash::make(explode(',', $first)[0]),
                'role' => explode(',', $first)[1],
                'status' => '1'
            ]);
        }
        
        foreach (range(1, 10) as $index){
            Driver::create([
                'nik' => $faker->randomNumber($nbDigits = NULL, $strict = false),
                'name' => $faker->name,
                'phone' => $faker->phoneNumber,
                'status' => '1'
            ]);
        }
    }
}
