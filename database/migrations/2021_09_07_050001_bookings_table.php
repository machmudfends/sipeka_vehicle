<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_admin');
            $table->unsignedBigInteger('id_vehicle');
            $table->unsignedBigInteger('id_driver');
            $table->char('status', 1)->comment('0: waiting | 1: approve | 2: reject')->nullable();
            $table->timestamps();

            $table->foreign('id_admin')->references('id')->on('users');
            $table->foreign('id_vehicle')->references('id')->on('vehicles');
            $table->foreign('id_driver')->references('id')->on('drivers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
