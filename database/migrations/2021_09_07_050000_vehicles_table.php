<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class VehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->char('type', 1)->comment('0: orang | 1: barang');
            $table->char('ownership', 1)->comment('0: perusahaan | 1: sewa');
            $table->integer('fuel_consumption');
            $table->char('service_calendar', 1)->comment('0: day | 1: month | 2: year');
            $table->integer('service_every');
            $table->char('status', 1)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
