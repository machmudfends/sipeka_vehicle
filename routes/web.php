<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DriverController;
use App\Http\Controllers\VehicleController;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\UserManageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', [AuthController::class, 'showFormLogin'])->name('login');
Route::post('/login', [AuthController::class, 'login'])->name('login.post');
// Route::get('/register', [AuthController::class, 'register'])->name('register');
Route::get('/logout', [AuthController::class, 'logout'])->name('logout');

Route::group([
    'middleware' => 'role:admin',
], function ($router) {
    // MASTER
    // drivers
    Route::get('/master/drivers', [DriverController::class, 'index'])->name('index.drivers');
    Route::get('/master/drivers/new', [DriverController::class, 'pageNew'])->name('new.drivers');
    Route::post('/master/drivers/new', [DriverController::class, 'postNew'])->name('postnew.drivers');
    Route::get('/master/drivers/edit/{id}', [DriverController::class, 'pageEdit'])->name('edit.drivers')->whereNumber('id');
    Route::post('/master/drivers/edit/{id}', [DriverController::class, 'postEdit'])->name('postedit.drivers')->whereNumber('id');
    Route::get('/master/drivers/delete/{id}', [DriverController::class, 'delete'])->name('delete.drivers')->whereNumber('id');
    Route::get('/master/drivers/delete/{ids}', [DriverController::class, 'bulkDelete'])->name('bulkdelete.drivers');
    // vehicles
    Route::get('/master/vehicles', [VehicleController::class, 'index'])->name('index.vehicles');
    Route::get('/master/vehicles/new', [VehicleController::class, 'pageNew'])->name('new.vehicles');
    Route::post('/master/vehicles/new', [VehicleController::class, 'postNew'])->name('postnew.vehicles');
    Route::get('/master/vehicles/edit/{id}', [VehicleController::class, 'pageEdit'])->name('edit.vehicles');
    Route::post('/master/vehicles/edit/{id}', [VehicleController::class, 'postEdit'])->name('postedit.vehicles')->whereNumber('id');
    Route::get('/master/vehicles/delete/{id}', [VehicleController::class, 'delete'])->name('delete.vehicles')->whereNumber('id');
    Route::get('/master/vehicles/delete/{ids}', [VehicleController::class, 'bulkDelete'])->name('bulkdelete.vehicles');
    
    // GENERAL
    Route::get('/master/bookings/new', [BookingController::class, 'pageNew'])->name('new.bookings');
    Route::post('/master/bookings/new', [BookingController::class, 'postNew'])->name('postnew.bookings');

    // USER MANAGEMENT
    Route::get('/master/users', [UserManageController::class, 'index'])->name('index.user');
    Route::get('/master/users/new', [UserManageController::class, 'pageNew'])->name('new.user');
    Route::post('/master/users/new', [UserManageController::class, 'postNew'])->name('postNew.user');
    Route::get('/master/users/edit/{id}', [UserManageController::class, 'pageEdit'])->name('edit.user')->whereNumber('id');
    Route::post('/master/users/edit/{id}', [UserManageController::class, 'postEdit'])->name('postedit.user')->whereNumber('id');
    Route::get('/master/users/delete/{id}', [UserManageController::class, 'delete'])->name('delete.user')->whereNumber('id');
    Route::get('/master/users/delete/{ids}', [UserManageController::class, 'bulkDelete'])->name('bulkdelete.user');
    
});

Route::group([
    'middleware' => 'role:approver',
], function ($router) {
    // GENERAL
    Route::get('/master/bookings/approve/{id}', [BookingController::class, 'approve'])->name('approve.bookings')->whereNumber('id');
    Route::get('/master/bookings/reject/{id}', [BookingController::class, 'reject'])->name('reject.bookings')->whereNumber('id');
});

Route::group([
    'middleware' => 'role:admin,approver',
], function ($router) {
    // DASHBOARD
    Route::get('/', [DashboardController::class, 'index'])->name('home');
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    
    // GENERAL
    Route::get('/master/bookings', [BookingController::class, 'index'])->name('index.bookings');

    // Report
    Route::get('/master/reports/bookingvehicle', [ReportController::class, 'bookingVehicle'])->name('bookingvehicle.reports');
    Route::get('/master/reports/bookingvehicle/excel', [ReportController::class, 'excel'])->name('excel.bookingvehicle.reports');
});