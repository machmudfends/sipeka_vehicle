### username:password
Role : Admin
- admin1:admin1
- admin2:admin2
- admin3:admin3

Role : Pihak Menyetujui
- approver1:approver1
- approver2:approver2
- approver3:approver3

### Database Version
mysql  Ver 8.0.26-0ubuntu0.20.04.2 for Linux on x86_64 ((Ubuntu))

### PHP Version
PHP 7.4.3 (cli) (built: Jul  5 2021 15:13:35) ( NTS )
Copyright (c) The PHP Group
Zend Engine v3.4.0, Copyright (c) Zend Technologies
    with Zend OPcache v7.4.3, Copyright (c), by Zend Technologies

### Framework
Laravel 8

## Instalataion
1. Install dependency
```bash
composer install
```

2. set up .env
```bash
cp .env.example .env
```

3. generate key
```bash
php artisan key:generate
```

4. Migrate & Seed
```bash
php artisan migrate --seed
```

5. Run
```bash
php artisan serve
```