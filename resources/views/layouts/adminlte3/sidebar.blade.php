@php
$user = Auth::user();
@endphp

<li class="nav-item">
  <a href="{{ Route('dashboard') }}" class="nav-link {{ Request::is('/') || Request::is('dashboard') ? 'active' : '' }}">
    <i class="nav-icon fas fa-tachometer-alt"></i>
    <p>
      Dashboard
    </p>
  </a>
</li>

@if ($user->role == '0')
<li class="nav-header">MASTER</li>
<li class="nav-item">
  <a href="{{ Route('index.drivers') }}" class="nav-link {{ Request::is('master/drivers') ? 'active' : '' }}">
    <i class="nav-icon fa fa-id-card"></i>
    <p>
      Pengemudi
    </p>
  </a>
</li>
<li class="nav-item">
  <a href="{{ Route('index.vehicles') }}" class="nav-link {{ Request::is('master/vehicles') ? 'active' : '' }}">
    <i class="nav-icon fas fa-truck"></i>
    <p>
      Kendaraan
    </p>
  </a>
</li>
@endif
<li class="nav-header">UMUM</li>
<li class="nav-item">
  <a href="{{ Route('index.bookings') }}" class="nav-link {{ Request::is('master/bookings') ? 'active' : '' }}">
    <i class="nav-icon fas fa-signature"></i>
    <p>
      Pemesanan Kendaraan
    </p>
  </a>
</li>
<li class="nav-item {{ Request::is('master/reports*') ? 'menu-open' : '' }}">
  <a href="#" class="nav-link">
    <i class="nav-icon fas fa-book {{ Request::is('master/reports*') ? 'active' : '' }}"></i>
    <p>
      Laporan
    <i class="fas fa-angle-left right"></i>
    </p>
  </a>
  <ul class="nav nav-treeview">
    <li class="nav-item">
      <a href="{{ Route('bookingvehicle.reports') }}" class="nav-link {{ Request::is('master/reports/bookingvehicle') ? 'active' : '' }}">
        <i class="far fa-circle nav-icon"></i>
        <p>Pemesanan Kendaraan</p>
      </a>
    </li>
  </ul>
</li>
@if ($user->role == '0')
<li class="nav-header">USER MANAGEMENT</li>
<li class="nav-item">
  <a href="{{ Route('index.user') }}" class="nav-link {{ Request::is('usermanagement/apps') ? 'active' : '' }}">
    <i class="nav-icon fas fa-users-cog"></i>
    <p> 
      User
    </p>
  </a>
</li>
@endif